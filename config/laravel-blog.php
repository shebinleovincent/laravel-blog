<?php

return [
	'blog'  => [
		'name'       => 'Blog', // name of the blog which will be displayed in the public listing page.
		'layout'     => 'vendor.laravel-blog.layout', // layout for all blog views
		'route_path' => 'blog', // route path for public blog entry listing (/blog) and blog entry page (/blog/{slug})
		'route_name' => 'blog', // route name for public blog entry listing (blog.index) and blog entry page (blog.show)
	],
	'admin' => [
		'name'       => 'Manage Blog', // name will be displayed in the manage page.
		'layout'     => 'vendor.laravel-blog.layout', // layout for all blog views
		'route_path' => 'admin/blog', // route path for admin pages for managing blog entries (/admin/blog)
		'route_name' => 'admin.blog', // route name for admin pages for managing blog entries (admin.blog.index)
		'middleware' => [], // add middleware to protect admin pages e.g. [ 'auth' ]
	],
];
