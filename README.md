# Laravel Blog

<p align="center">
    <a href="https://travis-ci.org/shebinleovincent/laravel-blog"><img src="https://travis-ci.org/shebinleovincent/laravel-blog.svg" alt="Build Status"></a>
    <a href="https://packagist.org/packages/shebinleovincent/laravel-blog"><img src="https://poser.pugx.org/shebinleovincent/laravel-blog/d/total.svg" alt="Total Downloads"></a>
    <a href="https://packagist.org/packages/shebinleovincent/laravel-blog"><img src="https://poser.pugx.org/shebinleovincent/laravel-blog/v/stable.svg" alt="Latest Stable Version"></a>
    <a href="https://packagist.org/packages/shebinleovincent/laravel-blog"><img src="https://poser.pugx.org/shebinleovincent/laravel-blog/license.svg" alt="License"></a>
</p>

Laravel 5 blog package with admin views for managing blog entries.

Take a look at [CONTRIBUTING.md](CONTRIBUTING.md) to see a to do list.

## Installation

Via Composer

``` bash
composer require shebinleovincent/laravel-blog
```

## Usage

### Run migration

```bash
php artisan migrate
```

### Publish config

```bash
php artisan vendor:publish --provider="ShebinLeoVincent\LaravelBlog\LaravelBlogServiceProvider"
```

### Config file

You can change the blog name, blog layout for views, blog route path for public listing and admin page using config file located in ```config/```.

```php
return [
	'blog'  => [
		'name'       => 'Blog', // name of the blog which will be displayed in the public listing page.
		'layout'     => 'vendor.laravel-blog.layout', // layout for all blog views
		'route_path' => 'blog', // route path for public blog entry listing (/blog) and blog entry page (/blog/{slug})
		'route_name' => 'blog', // route name for public blog entry listing (blog.index) and blog entry page (blog.show)
	],
	'admin' => [
		'name'       => 'Manage Blog', // name will be displayed in the manage page.
		'layout'     => 'vendor.laravel-blog.layout', // layout for all blog views
		'route_path' => 'admin/blog', // route path for admin pages for managing blog entries (/admin/blog)
		'route_name' => 'admin.blog', // route name for admin pages for managing blog entries (admin.blog.index)
		'middleware' => [], // add middleware to protect admin pages e.g. [ 'auth' ]
	],
];

```

### Blog routes

**/blog** - public route to list all blog entries.

**/admin/blog** - route to manage (create / edit / publish) the blog entries.

## Change log

Please see the [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details and a todolist.

## Security

If you discover any security related issues, please email shebinleovincent@gmail.com instead of using the issue tracker.

## Credits

- [Shebin Leo Vincent](https://gitlab.com/shebinleovincent)
- [All Contributors](CONTRIBUTING.md)

## License

MIT License. Please see the [license file](LICENSE) for more information.
