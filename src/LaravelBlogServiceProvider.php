<?php

namespace ShebinLeoVincent\LaravelBlog;

use Illuminate\Support\ServiceProvider;

class LaravelBlogServiceProvider extends ServiceProvider {
	/**
	 * Perform post-registration booting of services.
	 *
	 * @return void
	 */
	public function boot() {
		// $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'shebinleovincent');
		// $this->loadViewsFrom(__DIR__.'/../resources/views', 'shebinleovincent');
		$this->loadMigrationsFrom( __DIR__ . '/../database/migrations' );
		$this->loadRoutesFrom( __DIR__ . '/routes.php' );

		// Publishing is only necessary when using the CLI.
		if ( $this->app->runningInConsole() ) {
			$this->bootForConsole();
		}
	}

	/**
	 * Register any package services.
	 *
	 * @return void
	 */
	public function register() {
		$this->mergeConfigFrom( __DIR__ . '/../config/laravel-blog.php', 'laravel-blog' );

		// Register the service the package provides.
		$this->app->singleton( 'laravel-blog', function ( $app ) {
			return new LaravelBlog;
		} );
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides() {
		return [ 'laravel-blog' ];
	}

	/**
	 * Console-specific booting.
	 *
	 * @return void
	 */
	protected function bootForConsole() {
		// Publishing the configuration file.
		$this->publishes( [
			__DIR__ . '/../config/laravel-blog.php' => config_path( 'laravel-blog.php' ),
		], 'laravel-blog.config' );

		// Publishing the views.
		$this->publishes( [
			__DIR__ . '/../resources/views' => base_path( 'resources/views/vendor/laravel-blog' ),
		], 'laravel-blog.views' );

		// Publishing assets.
		$this->publishes( [
			__DIR__ . '/../resources/assets' => public_path( 'vendor/laravel-blog' ),
		], 'laravel-blog.views' );

		// Publishing the translation files.
		/*$this->publishes([
			__DIR__.'/../resources/lang' => resource_path('lang/vendor/shebinleovincent'),
		], 'laravel-blog.views');*/

		// Registering package commands.
		// $this->commands([]);
	}
}
