<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group( [ 'middleware' => [ 'web' ] ], function () {
	$blog_controller = 'ShebinLeoVincent\LaravelBlog\LaravelBlogController';
	$blog_route_path = config( 'laravel-blog.blog.route_path' );
	$blog_route_name = config( 'laravel-blog.blog.route_name' );
	Route::get( $blog_route_path, "$blog_controller@index" )->name( "$blog_route_name.index" );
	Route::get( "$blog_route_path/{slug}", "$blog_controller@show" )->name( "$blog_route_name.show" );

	$admin_controller = 'ShebinLeoVincent\LaravelBlog\LaravelBlogAdminController';
	$admin_route_path = config( 'laravel-blog.admin.route_path' );
	$admin_route_name = config( 'laravel-blog.admin.route_name' );
	Route::get( $admin_route_path, "$admin_controller@index" )->name( "$admin_route_name.index" );
	Route::get( "$admin_route_path/create", "$admin_controller@create" )->name( "$admin_route_name.create" );
	Route::post( $admin_route_path, "$admin_controller@store" )->name( "$admin_route_name.store" );
	Route::get( "$admin_route_path/{post}/edit", "$admin_controller@edit" )->name( "$admin_route_name.edit" );
	Route::put( "$admin_route_path/{post}", "$admin_controller@update" )->name( "$admin_route_name.update" );
	Route::delete( "$admin_route_path/{post}", "$admin_controller@delete" )->name( "$admin_route_name.delete" );
} );
