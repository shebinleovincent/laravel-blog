<?php
/**
 * Created by PhpStorm.
 * User: shebinleovincent
 * Date: 2019-02-14
 * Time: 23:33
 */

namespace ShebinLeoVincent\LaravelBlog;


use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model {

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'date',
		'title',
		'slug',
		'body',
		'excerpt',
		'image',
		'author',
		'views',
		'published',
		'sticky',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'date'
	];
}