<?php
/**
 * Created by PhpStorm.
 * User: shebinleovincent
 * Date: 2019-02-13
 * Time: 01:36
 */

namespace ShebinLeoVincent\LaravelBlog;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LaravelBlogController extends Controller {

	/**
	 * Show the application home page.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index( Request $request ) {
		$q     = $request->query( 'q' );
		$query = BlogPost::where( 'published', true )
			->orderBy( 'sticky', 'desc' )
			->orderBy( 'date', 'desc' );
		if ( ! empty( $q ) ) {
			$query->where( 'title', 'like', '%' . $request->query( 'q' ) . '%' );
			$query->orWhere( 'excerpt', 'like', '%' . $request->query( 'q' ) . '%' );
			$query->orWhere( 'body', 'like', '%' . $request->query( 'q' ) . '%' );
			$query->orWhere( 'tags', 'like', '%' . $request->query( 'q' ) . '%' );
			$query->orWhere( 'author', 'like', '%' . $request->query( 'q' ) . '%' );
		}
		$posts = $query->paginate( 10 );
		return view( 'vendor.laravel-blog.blog.index', compact( 'q', 'posts' ) );
	}


	/**
	 * Show the learn tip page.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( Request $request, $slug ) {
		$post = BlogPost::where( 'slug', $slug )->first();

		if ( is_null( $post ) ) {
			return abort( 404 );
		}

		if ( ! $post->published && ( ! Auth::check() || ( Auth::check() && ! Auth::user()->admin ) ) ) {
			return abort( 404 );
		}

		return view( 'vendor.laravel-blog.blog.show', compact( 'post' ) );
	}

}