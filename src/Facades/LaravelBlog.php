<?php

namespace ShebinLeoVincent\LaravelBlog\Facades;

use Illuminate\Support\Facades\Facade;

class LaravelBlog extends Facade {
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() {
		return 'laravel-blog';
	}
}
