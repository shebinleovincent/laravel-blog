<?php

namespace ShebinLeoVincent\LaravelBlog;

class LaravelBlog {

	public static function blogSitemap() {
		$entries = [];
		$posts   = BlogPost::where( 'published', true )->get();

		foreach ( $posts as $post ) {
			$entries[] = [
				'date'  => $post->date->toDateTimeString(),
				'image' => $post->image,
				'url'   => route( config( 'laravel-blog.blog.route_name' ) . '.show', $post->slug ),
			];
		}

		return $entries;
	}

}