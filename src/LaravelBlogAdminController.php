<?php
/**
 * Created by PhpStorm.
 * User: shebinleovincent
 * Date: 2019-02-17
 * Time: 01:54
 */

namespace ShebinLeoVincent\LaravelBlog;


use App\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LaravelBlogAdminController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct() {
		$middleware = config( 'laravel-blog.admin.middleware' );
		if ( ! empty( $middleware ) ) {
			$this->middleware( $middleware );
		}
	}

	/**
	 * Show the application home page.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index( Request $request ) {
		$q     = $request->query( 'q' );
		$query = BlogPost::where( 'published', true )
			->orderBy( 'sticky', 'desc' )
			->orderBy( 'date', 'desc' );
		if ( ! empty( $q ) ) {
			$query->where( 'title', 'like', '%' . $request->query( 'q' ) . '%' );
			$query->orWhere( 'excerpt', 'like', '%' . $request->query( 'q' ) . '%' );
			$query->orWhere( 'body', 'like', '%' . $request->query( 'q' ) . '%' );
			$query->orWhere( 'tags', 'like', '%' . $request->query( 'q' ) . '%' );
			$query->orWhere( 'author', 'like', '%' . $request->query( 'q' ) . '%' );
		}
		$posts = $query->paginate( 10 );
		return view( 'vendor.laravel-blog.admin.index', compact( 'q', 'posts' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$post            = new BlogPost;
		$post->sticky    = false;
		$post->published = true;
		$post->date      = Carbon::now()->toDateTimeString();

		return view( 'vendor.laravel-blog.admin.form', compact( 'post' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$post          = new BlogPost;
		$post->title   = $request->title;
		$post->slug    = str_slug( $request->title );
		$post->date    = $request->date;
		$post->author  = $request->author;
		$post->excerpt = $request->excerpt;
		$post->body    = $request->body;
		$post->tags    = preg_replace( '/\s*,\s*/', ',', $request->tags );
		$post->image   = $request->image;
//		if ( $request->hasFile( 'image' ) ) {
//			$post->image = $request->file( 'image' )->store( Constant::STORAGE_PUBLIC_TIP );
//		}
		$post->published = $request->published;
		$post->sticky    = $request->sticky;
		$post->save();

		$request->session()->flash( 'message', 'You have successfully added blog post.' );

		return redirect()->route( config( 'laravel-blog.admin.route_name' ) . '.index' );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param BlogPost $post
	 * @return \Illuminate\Http\Response
	 */
	public function edit( BlogPost $post ) {
		return view( 'vendor.laravel-blog.admin.form', compact( 'post' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, BlogPost $post ) {
		$post->title   = $request->title;
		$post->slug    = $request->slug;
		$post->date    = $request->date;
		$post->author  = $request->author;
		$post->excerpt = $request->excerpt;
		$post->body    = $request->body;
		$post->tags    = preg_replace( '/\s*,\s*/', ',', $request->tags );
		$post->image   = $request->image_old;
//		if ( $request->has( 'image_delete' ) ) {
//			Storage::delete( $post->image );
//			$post->image = null;
//		}
//		if ( $request->hasFile( 'image' ) ) {
//			$post->image = $request->file( 'image' )->store( Constant::STORAGE_PUBLIC_TIP );
//		}
		$post->published = $request->published;
		$post->sticky    = $request->sticky;
		$post->save();

		$request->session()->flash( 'message', 'You have successfully updated blog post.' );

		return redirect()->route( config( 'laravel-blog.admin.route_name' ) . '.edit', $post->id );
	}

}