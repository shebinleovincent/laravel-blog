@extends(config( 'laravel-blog.admin.layout' ))

@section('TITLE', ($post->id ? 'Edit' : 'Create').' Post')
@section('DESCRIPTION', ($post->id ? 'Edit' : 'Create').' Post')

@section('scripts')
    <script src="{{ asset('vendor/laravel-blog/js/ckeditor.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/laravel-blog/js/laravel-blog.js') }}" type="text/javascript"></script>
@endsection

@section('content')
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-12 blog-main">
                <h1 class="blog-post-title text-center">{{ ($post->id ? 'Edit' : 'Create').' Post' }}</h1>

                <form class="" role="form" method="POST" enctype="multipart/form-data"
                      action="{{ $post->id ? route(config( 'laravel-blog.admin.route_name' ) . '.update', $post->id) : route(config( 'laravel-blog.admin.route_name' ) . '.store') }}">
                    {{ csrf_field() }}
                    @if($post->id)
                        {{ method_field('PUT') }}
                    @endif

                    <div class="row">
                        <div class="form-group col-sm-12 {{ $errors->has('title') ? 'has-error' : false }}">
                            <label for="title" class="control-label required">Title</label>
                            <input id="title" class="form-control" name="title" placeholder="Title"
                                   required value="{{ old('title', $post->title) }}">
                        </div>
                    </div>

                    @if($post->id)
                        <div class="row">
                            <div class="form-group col-sm-12 {{ $errors->has('slug') ? 'has-error' : false }}">
                                <label for="slug" class="control-label required">Slug</label>
                                <input id="slug" class="form-control" name="slug" placeholder="URL"
                                       required value="{{ old('slug', $post->slug) }}">
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('published') ? 'has-error' : false }}">
                            <label for="published" class="control-label required">Published</label>
                            <div>
                                <div class="radio d-inline">
                                    <input type="radio" name="published" id="published_1"
                                           value="1" {{ old('published', $post->published) == '1' ? 'checked' : '' }}>
                                    <label class="radio-inline" for="published_1">Yes</label>
                                </div>
                                <div class="radio d-inline">
                                    <input type="radio" name="published" id="published_0"
                                           value="0" {{ old('published', $post->published) == '0' ? 'checked' : '' }}>
                                    <label class="radio-inline" for="published_0">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-sm-6 {{ $errors->has('sticky') ? 'has-error' : false }}">
                            <label for="sticky" class="control-label required">Sticky</label>
                            <div>
                                <div class="radio d-inline">
                                    <input type="radio" name="sticky" id="sticky_1"
                                           value="1" {{ old('sticky', $post->sticky) == '1' ? 'checked' : '' }}>
                                    <label class="radio-inline" for="sticky_1">Yes</label>
                                </div>
                                <div class="radio d-inline">
                                    <input type="radio" name="sticky" id="sticky_0"
                                           value="0" {{ old('sticky', $post->sticky) == '0' ? 'checked' : '' }}>
                                    <label class="radio-inline" for="sticky_0">No</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('date') ? 'has-error' : false }}">
                            <label for="date" class="control-label required">Date</label>
                            <input id="date" class="form-control" name="date" placeholder="Date"
                                   required value="{{ old('date', $post->date) }}">
                        </div>
                        <div class="form-group col-sm-6 {{ $errors->has('image') ? 'has-error' : false }}">
                            <label for="image" class="control-label">Image</label>
                            <div>
                                <input type="hidden" name="image_old" value="{{ $post->image }}">
                                <input id="image" type="file" name="image">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('tags') ? 'has-error' : false }}">
                            <label for="tags" class="control-label required">Tags</label>
                            <input id="tags" class="form-control" name="tags" placeholder="Tags"
                                   required value="{{ old('tags', $post->tags) }}">
                        </div>
                        <div class="form-group col-sm-6 {{ $errors->has('author') ? 'has-error' : false }}">
                            <label for="author" class="control-label required">Author</label>
                            <input id="author" class="form-control" name="author"
                                   placeholder="Author" required
                                   value="{{ old('author', $post->author) }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12 {{ $errors->has('excerpt') ? 'has-error' : false }}">
                            <label for="excerpt" class="control-label required">Excerpt</label>
                            <textarea id="excerpt" type="text" class="form-control" name="excerpt"
                                      placeholder="Excerpt"
                                      rows="5"
                                      required>{{ old('excerpt', $post->excerpt) }}</textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12 {{ $errors->has('body') ? 'has-error' : false }}">
                            <label for="body" class="control-label">Content</label>
                            <textarea id="body" type="text" class="editable" name="body"
                                      placeholder="Content">{{ old('body', $post->body) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group mt-4">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit"
                                    class="btn btn-primary">{{ $post->id ? 'Update' : 'Create' }} Post
                            </button>
                            <a href="{{ route(config( 'laravel-blog.admin.route_name' ) . '.index') }}"
                               title="Cancel" class="btn btn-secondary">Cancel</a>
                            @if($post->id)
                                <a href="{{ route(config( 'laravel-blog.blog.route_name' ) . '.show', $post->slug) }}"
                                   target="_blank" title="View" class="btn btn-info">View Post</a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection