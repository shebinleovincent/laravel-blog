<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>@yield('TITLE')</title>
    <meta name="description" content="@yield('DESCRIPTION')">
    <meta name='author' content='@yield('AUTHOR')'>
    <meta property="date" content="@yield('DATE')">
    <meta property="og:title" content="@yield('TITLE')"/>
    <meta property="og:description" content="@yield('DESCRIPTION')"/>
    <meta property="og:url" content="{{ url()->current() }}"/>
    <meta property="og:image" content="@yield('IMAGE')"/>

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="@yield('TITLE')"/>
    <meta name="twitter:description" content="@yield('DESCRIPTION')"/>
    <meta name="twitter:url" content="{{ url()->current() }}"/>
    <meta name="twitter:image" content="@yield('IMAGE')"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    {{--<!-- CSS Files -->--}}
    <link href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="{{ asset('vendor/laravel-blog/css/laravel-blog.css') }}" rel="stylesheet"/>
    @yield('styles')
</head>
<body>
    <div class="container">
        <header class="blog-header py-3">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-12 pt-1">
                    <a class="blog-header-logo text-muted" href="/">Home</a>
                </div>
            </div>
        </header>
    </div>

    @yield('content')

    <footer class="blog-footer">
        <p>Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://www.vinzcorp.com/">VinzCorp
                Solutions</a>.</p>
    </footer>
    {{--<!--   Core JS Files   -->--}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"></script>
    @yield('scripts')
</body>
</html>
