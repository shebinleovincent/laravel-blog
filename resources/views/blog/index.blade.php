@extends(config( 'laravel-blog.blog.layout' ))

@section('TITLE', config( 'laravel-blog.blog.name' ))
@section('DESCRIPTION', config( 'laravel-blog.blog.name' ))

@section('content')
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-12 blog-main">
                <h1 class="text-center m-3">{{ config( 'laravel-blog.blog.name' ) }}</h1>

                <form class="form-horizontal mt-lg-2 mb-lg-4" role="form" method="GET"
                      action="{{ route(config( 'laravel-blog.blog.route_name' ) . '.index') }}">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search"
                               value="{{ $q }}">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                </form>

                @foreach($posts as $index => $post)
                    <div class="blog-post">
                        <a href="{{ route(config( 'laravel-blog.blog.route_name' ) . '.show', $post->slug) }}" class="text-dark">
                            <h2 class="blog-post-title">{{ $post->title }}</h2>
                        </a>
                        <p class="blog-post-meta">{{ $post->date->format('d F Y') }} by <a href="#">{{ $post->author }}</a></p>
                        {!! nl2br($post->excerpt) !!}
                        <a href="{{ route(config( 'laravel-blog.blog.route_name' ) . '.show', $post->slug) }}">
                            read more...</a><br>
                        @php $tags = explode(',', $post->tags);@endphp
                        @foreach($tags as $tag)
                            <a href="#"><span class="badge badge-primary">{{ $tag }}</span></a>
                        @endforeach
                    </div>
                @endforeach

                {!! $posts->appends(['q' => $q])->links() !!}

            </div>
        </div>
    </main>
@endsection