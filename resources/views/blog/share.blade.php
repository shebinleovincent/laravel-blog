@section('styles')
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet"
          href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-minima.css"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    <script>
        $(".jssocials").jsSocials({
            {!! isset($share_url) ? "url: \"$share_url\"," : '' !!}
            shares: [{share: 'facebook', label: 'Share'}, "whatsapp", "telegram", "twitter", "googleplus", "email"],
            showCount: true
        });
    </script>
@endsection

<div class="jssocials {{ isset($share_class) ? $share_class : '' }} mb-2 mt-2"></div>
