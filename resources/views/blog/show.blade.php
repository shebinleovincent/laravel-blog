@extends(config( 'laravel-blog.blog.layout' ))

@section('TITLE', $post->title)
@section('DESCRIPTION', $post->excerpt)
@section('AUTHOR', $post->author)
@section('DATE', $post->date->format('d F Y'))

@section('content')
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-12 blog-main">
                <div class="text-center mt-2">
                    <h1 class="blog-post-title">{{ $post->title }}</h1>
                    <p class="blog-post-meta">{{ $post->date->format('d F Y') }} by <a href="#">{{ $post->author }}</a></p>
                </div>

                @include('vendor.laravel-blog.blog.share')

                <div class="mt-4">{!! $post->body !!}</div>

                <div class="mb-4">
                    @php $tags = explode(',', $post->tags);@endphp
                    @foreach($tags as $tag)
                        <span class="badge badge-info">{{ $tag }}</span>
                    @endforeach
                </div>

                @include('vendor.laravel-blog.blog.share')
            </div>
        </div>
    </main>
@endsection