$(function () {
    if (typeof ClassicEditor !== 'undefined') {
        ClassicEditor
            .create(document.querySelector('.editable'))
            .catch(function (error) {
                console.error(error);
            });
    }
});