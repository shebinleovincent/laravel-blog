# Changelog

All notable changes to `laravel-blog` will be documented in this file.

## Version 1.0.2

### Added
- Added layout with bootstrap blog theme

## Version 1.0.1

### Added
- Removed seotools dependencies to keep it simple

## Version 1.0

### Added
- Everything
