<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'blog_posts', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->datetime( 'date' );
			$table->string( 'title' );
			$table->string( 'slug' );
			$table->longText( 'body' );
			$table->text( 'excerpt' );
			$table->string( 'tags' )->nullable();
			$table->string( 'image' )->nullable();
			$table->string( 'author' )->nullable();
			$table->integer( 'views' )->default( 0 );
			$table->boolean( 'sticky' )->default( false );
			$table->boolean( 'published' )->default( true );
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'blog_posts' );
	}
}
